import "./App.scss";

import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "./pages/homePage/homePage";
import ElementPage from "./pages/element/element";
import GalleryPage from "./pages/galleryPage/galleryPage";
import AboutPage from "./pages/aboutPage/aboutPage";
import React, { useEffect, useState } from "react";
import GestionPage from "./pages/gestionPage/gestionPage";
import ListApi from "./components/listApi/listApi";

export const cartHook = React.createContext(); //Contexto

export const apiHook = React.createContext(); //Contexto


function App() {

  //Const globales
  const [cart, setCart] = useState([]); //Estado

  //API
  const [list, setList] = useState([]);

  const resApiFn = async () => {
      const resApi = await ListApi();
      setList(resApi.data.apartments);
      console.log(resApi.data.apartments);
  };

  useEffect(() => {resApiFn()}, []);

  return (
    <div className="App">
      <BrowserRouter>
      <cartHook.Provider value={cart}>
      <apiHook.Provider value={list}>
        <Routes>
          <Route path="/">
            <Route index element={<HomePage />} />
            <Route path="gallery">
              <Route index element={<GalleryPage setCart = {setCart} list={list} />} />
              <Route path=":element" element={<ElementPage />} />
            </Route>
            <Route path="form">
              <Route index element={<AboutPage list={list} setList={setList}/>} />
            </Route>
            <Route path="gestion">
              <Route index element={< GestionPage setCart = {setCart} />} />
            </Route>
          </Route>
        </Routes>
        </apiHook.Provider>
        </cartHook.Provider>
      </BrowserRouter>
    </div>
  );
}

export default App;
