import "./list.scss";
import { useState } from "react"


//Bootstrap
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'

export default function List ({setCart, list}) {


    //FINDER
    const [inputValue, setInputValue] = useState('');

    const [showOrNotCityFound, setShowOrNotCityFound] = useState(false);

    const [cityFound, setCityFound] = useState({
        ciudad: '',
        imagen: '',
        id: 0    
    });

    const onChangeFunction = (ev) => {
        setInputValue(ev.target.value)
    };

    const onFinderFunction = () => {
        list.map(element => {
            if(element.ciudad === inputValue){
                setShowOrNotCityFound(true);
                setCityFound(element);
                setInputValue('');
            return console.log(element)}
        });
    }

    //Cart
    const [cartOfList, setCartOfList] = useState([]);

    const addItem = (element) => {
        cartOfList.push(element)
        console.log(cartOfList);
        setCart(cartOfList);
    };

    return (
        <>
        <div className="finder">
        <input className="finder__input"
        placeholder='Filtra por ciudad'
        type="text"
        value={inputValue} 
        onChange={onChangeFunction}
        />
        <button className="finder__button" onClick={onFinderFunction}>Buscar</button>
        </div>
        {showOrNotCityFound && 
                <div className="finder__found">
                    <h4>Hemos encontrado esto:</h4>
                    <Card  bg={'danger'} style={{ width: '18rem' , margin: '15px'}}>
                        <Card.Img className="list__cards__card__img" variant="top" src={cityFound.imagen} />
                        <Card.Body>
                        <Card.Title>Apartamento en {cityFound.ciudad}</Card.Title>
                        <div>
                            <p>Código: {cityFound.id}</p>
                            <p>Este apartmento ofrece...</p>
                        </div>
                        <Button onClick={()=>addItem(cityFound)} variant="light">Reservar</Button>
                        </Card.Body>
                    </Card> 
                </div>}
        <div className="list">
            <h2 className="list__title">Apartamentos</h2>
            <div className="list__cards">
            {list.map((element, index) =>
                <div className="list__cards__card" key={index}>
                    <Card  bg={'secondary'} style={{ width: '18rem' }}>
                        <Card.Img className="list__cards__card__img" variant="top" src={element.imagen} />
                        <Card.Body>
                        <Card.Title>Apartamento en {element.ciudad}</Card.Title>
                        <div>
                            <p>Código: {element.id}</p>
                            <p>Este apartmento ofrece...</p>
                        </div>
                        <Button onClick={()=>addItem(element)} variant="light">Reservar</Button>
                        </Card.Body>
                    </Card> 
                </div>
            )}
            </div>
        </div>
        </>
    );
};