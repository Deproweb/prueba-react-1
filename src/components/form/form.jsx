import { axios } from "axios";
import React from "react";
import { useForm } from "react-hook-form";
import "./form.scss";
import Button from 'react-bootstrap/Button';


export default function Form ({list, setList}) {

    const { register, handleSubmit, formState: { errors } } = useForm();
    
    const onFormSubmit = async (data) => {
        console.log(data);
        setList([data, ...list])
    }

    return (
        <div className="c-form">
            <h3>Formulario de contacto</h3>
        <form onSubmit={handleSubmit(onFormSubmit)}>

        <div>
            <label htmlFor="ciudad"></label>
            <input className="c-form__input" id="ciudad" placeholder="Ciudad" {...register("ciudad")} />
        </div>

        <div>
            <label htmlFor="imagen"></label>
            <input className="c-form__input" id="imagen" placeholder="Imagen" {...register("imagen")} />
        </div>

        <div>
            <label htmlFor="id"></label>
            <input className="c-form__input" id="id" placeholder="ID" {...register("id", { required: true })} />
            {errors.id && <span>Campo requerido*</span>}
        </div>
        
        <Button variant="light">Enviar</Button>
        </form>
        </div>
    )    
}