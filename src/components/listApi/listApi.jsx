import axios from "axios";

export default function ListApi() {

    return axios.get("https://my-json-server.typicode.com/Deproweb/apartment/db");

};

