import "./header.scss";

//Bootstrap

import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";

export default function Header() {
    return (
        <div className="header">
            <div className="header__logo">
                Upgrade Vacation
            </div>
            <div className="header__nav">
                <Nav className="justify-content-center" activeKey="/home">
                    <Nav.Item>
                        <Link className="header__nav__link" to="/">Home</Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Link className="header__nav__link" to="/gallery">Gallery</Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Link className="header__nav__link" to="/form">Form</Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Link className="header__nav__link" to="/gestion">Cart</Link>
                    </Nav.Item>
                </Nav>
            </div>
        </div>
    );
}
