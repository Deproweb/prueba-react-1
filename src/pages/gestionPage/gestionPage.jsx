import { useContext} from "react"
import  Button from "react-bootstrap/Button"
import { cartHook } from "../../App"
import Footer from "../../components/footer/footer"
import Header from "../../components/header/header"
import "./gestionPage.scss"
import Card from 'react-bootstrap/Card'


export default function GestionPage({setCart}) {

    const cartMaybe = useContext(cartHook); 

    const deleteItem = (index) => {
        const cartCopy = [...cartMaybe];
        cartCopy.splice(index, 1);
        setCart(cartCopy);
    };

    return(
        <div>
        <Header></Header>
        {cartMaybe.length === 0 && <div>
            <h3>Todavía no hay nada en el carrito</h3>
            <h5>Ve a nuestro apartado de ofertas y elige el que más te guste</h5>
            </div>}
        {cartMaybe.length > 0 && <div className="cart">
            {cartMaybe.map(element => 
            <Card  bg={'light'} style={{ width: '10rem', height: '18rem' , margin: '10px'}}>
                <Card.Img className="list__cards__card__img" variant="top" src={element.imagen} />
                <Card.Body>
                <Card.Title>Apartamento en {element.ciudad}</Card.Title>
                <div>
                    <p>Código: {element.id}</p>
                </div>
                <Button onClick={()=>deleteItem(element)} variant="danger">Eliminar</Button>
                </Card.Body>
            </Card> 
            )}
        </div>}
        <Footer></Footer>
        </div>
    )

}