
//Bootstrap

import Footer from "../../components/footer/footer"
import Form from "../../components/form/form"

//Components
import Header from "../../components/header/header"

export default function AboutPage({list, setList}) {

    return(
        <div>
            <Header></Header>
            <Form setList={setList} list={list}></Form>
            <Footer></Footer>
        </div>
    )
} 