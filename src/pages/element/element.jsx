import { useParams } from "react-router";
import Header from "../../components/header/header";

export default function ElementPage () {

    const {element} = useParams("element")

    return (
        <div>
        <Header></Header>
        <div>Soy el element con parámetro {element}</div>
        </div>
    )

}