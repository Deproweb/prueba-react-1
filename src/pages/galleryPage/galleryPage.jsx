import Footer from "../../components/footer/footer";
import Gallery from "../../components/gallery/gallery";
import Header from "../../components/header/header";
import List from "../../components/list/list";

export default function GalleryPage ({setCart, list}) {

    return (
        <div>
        <Header></Header>
        <Gallery></Gallery>
        <List setCart = {setCart} list={list} ></List>
        <Footer></Footer>
        </div>
    );

};
