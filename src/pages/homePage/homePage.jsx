
import CarouselComponent from "../../components/carousel/carousel";
import Footer from "../../components/footer/footer";
import Header from "../../components/header/header";

export default function HomePage () {

    return (
        <>
        <Header></Header>
        <CarouselComponent></CarouselComponent>
        <Footer></Footer>
        </>
    )

}